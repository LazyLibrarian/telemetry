# Server object for LazyLibrarian telemetry server

import yaml
import logging
import logging.config
import configparser
from bottle import Bottle, request, response
import telemetrydb
import telemetryweb
import time
import threading
import datetime
from functools import wraps
from zipfile import ZipFile
import os
import shutil
from rclone_python import rclone


def bottle_to_logger(fn):
    """ Helper function, making Bottle logging go to our logger """

    @wraps(fn)
    def _log_to_logger(*args, **kwargs):
        request_time = datetime.datetime.now()
        actual_response = fn(*args, **kwargs)
        # modify this to log exactly what you need:
        _logger.info('BOB %s %s %s %s %s' % (request.remote_addr,
                                             request_time,
                                             request.method,
                                             request.url,
                                             response.status))
        return actual_response

    return _log_to_logger


class TelemetryServer:
    config: configparser.ConfigParser

    def every_n_hours(self, delay, task):
        delay = delay * 60 * 60
        next_time = time.time() + delay
        dt = datetime.datetime.utcfromtimestamp(next_time).strftime('%Y-%m-%d %H:%M:%S')
        self.logger.info(f"Next backup UTC {dt}")
        while True:
            time.sleep(max(0, next_time - time.time()))
            self.logger.debug('Starting Task') 
            try:
                task()
            except Exception as e:
                self.logger.error('Task failed')
                self.logger.error(str(e))   
            finally:
                next_time += (time.time() - next_time) // delay * delay + delay 
                dt = datetime.datetime.utcfromtimestamp(next_time).strftime('%Y-%m-%d %H:%M:%S')
                self.logger.info(f"Next backup UTC {dt}")
 
    def copy_telemetrydb(self):
        self.logger.debug(f'Copying telemetry data')
        dbname = '/data/' + self.config.get('database', 'dbname', fallback='LazyTelemetry')
        nowstr = f"{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}"
        new_db = f"/data/{nowstr}_telemetry.db"
        shutil.copyfile(dbname, new_db)
        self.logger.debug(f"copied to {new_db}")
        return new_db

    def zip_up(self, filename):
        try:
            of = filename + '.zip'
            with ZipFile(of, 'w') as zf:
                zf.write(filename)
                self.logger.debug(f'Zipped {filename}')
        except Exception as e:
            self.logger.error(f'Zipping {filename} failed')
            self.logger.error(str(e))
            of = ''
        finally:
            os.remove(filename)
        return of

    def send_to_dropbox(self, filename):
        try:
            rclone.copy(filename, f'dropbox:Apps/LazyLibrarian/data/{filename}', ignore_existing=True, args=['--create-empty-src-dirs'])
        except Exception as e:
            self.logger.error(f'Sending {filename} failed')
            self.logger.error(str(e))
        finally:
            return filename
        """
        app_key = self.config.get('dropbox', 'app_key', fallback='')
        app_secret = self.config.get('dropbox', 'app_secret', fallback='')
        refresh_token = self.config.get('dropbox', 'refresh_token', fallback='')
        meta = None
        if app_key and app_secret and refresh_token:
            try:
                dbx = dropbox.Dropbox(
                                        app_key=app_key,
                                        app_secret=app_secret,
                                        oauth2_refresh_token=refresh_token
                                        )
                with open(filename, 'rb') as f:
                    meta = dbx.files_upload(f.read(), f'{filename}', mute=True)
                    self.logger.debug(f'Sent {filename}')
            except Exception as e:
                self.logger.error(f'Sending {filename} failed')
                self.logger.error(str(e))
            finally:
                return meta
        """

    def backup_and_send(self):
        self.logger.info("Backing up telemetry")
        new_db = self.copy_telemetrydb()
        zf = self.zip_up(new_db)
        if zf:
            meta = self.send_to_dropbox(zf)
            self.logger.debug(str(meta))
            os.remove(zf)

    def __init__(self):
        self._telemetry_db = None

    def initialize(self):
        # Parse command line
        # Read config file
        self.config = configparser.ConfigParser()
        self.config.read('telemetry.ini')

        self._initlogger()
        app = Bottle()
        app.install(bottle_to_logger)

    def _initlogger(self):
        with open("tslogging.yaml", "r") as stream:
            try:
                logsettings = yaml.safe_load(stream)
                logging.config.dictConfig(logsettings)
            except yaml.YAMLError as exc:
                print(f"YAML error reading logging config: {str(exc)}")
            except Exception as e:
                print(f"Error reading logging config: {str(e)}")
        self.logger = logging.getLogger(__name__)
        self.logger.disabled = False
        self.logger.info('Starting LazyLibrarian telemetry server')

    def start(self):
        # Open the database and start the server
        self._telemetry_db = telemetrydb.TelemetryDB(self.config)
        self.logger.debug('Initializing database')
        if self._telemetry_db.initialize():
            frequency = self.config.getint('dropbox', 'hours', fallback=0)
            if frequency:
                self.logger.info('Start backup thread')
                threading.Thread(target=lambda: self.every_n_hours(frequency, self.backup_and_send)).start()
            self.logger.debug('Run server')
            telemetryweb.run_server(self._telemetry_db.add_telemetry, self._telemetry_db.read_telemetry,
                                    self._telemetry_db.read_csv)

    def stop(self):
        self.logger.debug('Stopping server')
        self._telemetry_db = None  # Closes the database


_logger = logging.getLogger(__name__)
