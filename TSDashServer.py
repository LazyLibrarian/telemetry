import logging
from hashlib import md5

from io import StringIO
import json
import pandas as pd
import os
import requests
from http.client import responses
from dash import Dash, html, dcc, Input, Output
import plotly.express as px
import plotly.graph_objects as go

TELEMETRY_SERVER = 'https://lazylibrarian.telem.ch/'
intervals = ['Day', 'Hour', 'Week', 'Month']

time_dropdown = dcc.Dropdown(options=intervals, value='Day')


def getcachefilename(url: str) -> str:
    if not os.path.isdir('./cache'):
        os.makedirs('./cache')
    filehash = md5(url.encode('utf-8'))
    cachefilename = os.path.join('./cache', filehash.hexdigest())
    return cachefilename


def do_cache(url: str, res: str):
    if 'https://gitlab.com' not in url:
        return  # Only cache gitlab requests

    cachefilename = getcachefilename(url)
    fp = open(cachefilename, 'w')
    try:
        fp.write(res)
    finally:
        fp.close()


def in_cache(url: str) -> (str, bool):
    if 'https://gitlab.com' not in url:
        return '', False # Only cache gitlab requests

    res = ''
    found = False
    cachefilename = getcachefilename(url)
    if os.path.isfile(cachefilename):
        try:
            fp = open(cachefilename, 'r')
            res = fp.read()
            fp.close()
            found = res != ''
        except Exception:
            found = False
            pass

    return res, found


def server_request(url: str) -> (str, bool):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    proxies = {}
    timeout = 5
    headers = {'User-Agent': 'LazyLibrarian'}
    if proxies:
        payload = {"timeout": timeout, "proxies": proxies}
    else:
        payload = {"timeout": timeout}
    try:
        cached, incache = in_cache(url)
        if incache:
            return cached, True
        else:
            logger.info(f'GET {url}')
            r = requests.get(url, verify=False, params=payload, headers=headers)
    except requests.exceptions.Timeout as e:
        logger.error("_send_url: Timeout sending telemetry %s" % url)
        return "Timeout %s" % str(e), False
    except Exception as e:
        return "Exception %s: %s" % (type(e).__name__, str(e)), False

    if str(r.status_code).startswith('2'):  # (200 OK etc)
        do_cache(url, r.text)
        return r.text, True  # Success
    if r.status_code in responses:
        msg = responses[r.status_code]
    else:
        msg = r.text
    return "Response status %s: %s" % (r.status_code, msg), False


def get_timeline_telemetry(name: str, granularity: str) -> (str, bool):
    return server_request(f'{TELEMETRY_SERVER}/csv/{name}/{granularity}')


def get_stats_telemetry(name: str) -> (str, bool):
    return server_request(f'{TELEMETRY_SERVER}/stats/{name}')


json_str, ok = get_stats_telemetry('servers')
if ok:
    json_data = json.loads(json_str)
    python_justver = json_data['servers']['python_justver']
    # Ignore the revision number:
    justmajorminor = {}
    for fullver in python_justver:
        splitver = fullver.split('.')
        ver = f"{splitver[0]}.{splitver[1]}"
        if ver in justmajorminor:
            justmajorminor[ver] += python_justver[fullver]
        else:
            justmajorminor[ver] = python_justver[fullver]

    dataframe = pd.DataFrame.from_dict(justmajorminor, orient='index', columns=['count'])
    python_distribution = px.pie(dataframe, names=dataframe.index, values='count', title='Python versions')

    oses = json_data['servers']['os']
    dataframe = pd.DataFrame.from_dict(oses, orient='index', columns=['count'])
    os_distribution = px.pie(dataframe, names=dataframe.index, values='count', title='OS distribution')

    installtypes = json_data['servers']['ll_installtype']
    dataframe = pd.DataFrame.from_dict(installtypes, orient='index', columns=['count'])
    installtype_distribution = px.pie(dataframe, names=dataframe.index, values='count', title='Installation types')

    # Get the versions in a single format
    versions = json_data['servers']['ll_version']
    short_versions = {}
    for key, value in versions.items():
        short_key = key[:8]
        if short_key in short_versions:
            short_versions[short_key] += value
        else:
            short_versions[short_key] = value
    # Look up the date/time of the versions
    dateversions = {}
    for version, count in short_versions.items():
        vdatastr, ok = server_request(f'https://gitlab.com/api/v4/projects/LazyLibrarian%2Flazylibrarian/repository/commits/{version}')
        if ok:
            vdata = json.loads(vdatastr)
            datestr = vdata['authored_date'][:10]  # Grab just the date, ignore time and TZ
            if datestr < '2023-01-01':  # Can't be before telemetry appeared
                datestr = '2023-01-01'
        else:
            datestr = '2022-12-31'  # Invalid version string
        if datestr in dateversions:
            dateversions[datestr] += count
        else:
            dateversions[datestr] = count
    sorted_versions = dict(sorted(dateversions.items()))
    dataframe = pd.DataFrame.from_dict(sorted_versions, orient='index', columns=['count'])
    version_distribution = px.bar(dataframe, x=dataframe.index, y='count', title='Version age')
else:
    python_distribution = None
    os_distribution = None
    version_distribution = None


app = Dash()
app.layout = html.Div(children=[
    html.H1(children='LazyLibrarian usage statistics'),
    html.Div(children=[
        html.Div(style={'display': 'flex'}, children=[
            dcc.Graph(id='installtype_distribution', figure=installtype_distribution),
            dcc.Graph(id='python_distribution', figure=python_distribution),
            dcc.Graph(id='version_distribution', figure=version_distribution),
        ]),
    ]),
    html.Div(children=[
        html.H2(children='Reports over time'),
        time_dropdown,
        dcc.Graph(id='unique_over_time')
    ])
])


@app.callback(
    Output(component_id='unique_over_time', component_property='figure'),
    Input(component_id=time_dropdown, component_property='value')
)
def update_timeline(selected_granularity):
    csv_data, ok = get_timeline_telemetry('servers', selected_granularity)
    if ok:
        dataframe = pd.read_csv(StringIO(csv_data))
        line_fig = px.line(dataframe,
                           x='date', y='reports',
                           title=f'LazyLibrarian reports per {selected_granularity}')
        return line_fig
    else:
        return 'Oops'


app.run_server(debug=True, use_reloader=True)
